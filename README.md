# wiqr 

`wiqr` is a small R package acting as a simple SPARQL client for
Wikidata Query Service.

This package's source code will be moved to CRAN package
[wikifacts](https://cran.r-project.org/package=wikifacts) soon, so I recommend
to use that if it’s suitable for your needs.

## Installation

You can install the latest version of `wiqr` with:

``` r
# maybe you'll need to install 'remotes' package first
# install.packages("remotes")

remotes::install_bitbucket("wikiskripta/wiqr")
```

## Example

This is a basic example which shows you how to obtain a data frame with
information about Czech physicians from Wikidata:

``` r
library(wiqr)

# get list of Czech physicians from Wikidata
sparql <- '
  SELECT ?physician ?physicianLabel ?birthPlaceLabel ?birthDate ?deathDate
    WHERE {
      ?physician wdt:P106 wd:Q39631;
                 wdt:P27 wd:Q213 .
    OPTIONAL { ?physician wdt:P19 ?birthPlace . }
    OPTIONAL { ?physician wdt:P569 ?birthDate . }
    OPTIONAL { ?physician wdt:P570 ?deathDate . }
    SERVICE wikibase:label {
      bd:serviceParam wikibase:language "[AUTO_LANGUAGE], en" .
    }
  }
'
czech_physicians <- wikidata(sparql)

str(czech_physicians)
#> 'data.frame':    301 obs. of  5 variables:
#>  $ physician      : chr  "http://www.wikidata.org/entity/Q231231" "http://www.wikidata.org/entity/Q287879" "http://www.wikidata.org/entity/Q5443861" "http://www.wikidata.org/entity/Q3512181" ...
#>  $ physicianLabel : chr  "Zuzana Roithová" "Přemysl Sobotka" "Ferdinand Knobloch" "Q3512181" ...
#>  $ birthPlaceLabel: chr  "Prague" "Mladá Boleslav" "Prague" "Prague" ...
#>  $ birthDate      : chr  "1953-01-30T00:00:00Z" "1944-05-18T00:00:00Z" "1916-08-15T00:00:00Z" "1963-07-05T00:00:00Z" ...
#>  $ deathDate      : chr  NA NA "2018-01-19T00:00:00Z" NA ...
```

## License

Created and maintained by Petr Kajzar, 2019-2020.

The package is provided under
[Creative Commons Zero v1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode.txt)
license.
